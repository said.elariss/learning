package io.xhub.Bootcamp.enums;

public enum Status {
    PENDING,
    ACCEPTED,
    REJECTED
}
