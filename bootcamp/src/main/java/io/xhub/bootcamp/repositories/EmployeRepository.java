package io.xhub.Bootcamp.repositories;

import io.xhub.Bootcamp.domains.Employe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface  EmployeRepository extends JpaRepository<Employe,String> {
    Optional<Employe> findByEmail(String email);
}
