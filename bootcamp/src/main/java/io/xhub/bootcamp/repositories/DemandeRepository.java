package io.xhub.Bootcamp.repositories;

import io.xhub.Bootcamp.domains.Demande;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.Root;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.ArrayList;

public interface DemandeRepository extends JpaRepository<Demande,String> {
}
