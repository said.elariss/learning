package io.xhub.Bootcamp.repositories;

import io.xhub.Bootcamp.domains.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AdminRepository extends JpaRepository<Admin,String> {
    Optional<Admin> findByUsername(String username);

}
