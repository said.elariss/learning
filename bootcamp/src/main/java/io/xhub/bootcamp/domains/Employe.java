package io.xhub.Bootcamp.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.xhub.Bootcamp.enums.Type;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Employe {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    private String firstName;
    private String password;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String birthDate;
    private String adress;
    private String cnss;
    private String cin;
    private String hiringDate;
    private Type type;
    private Integer salary;
    @OneToMany(mappedBy = "employe",fetch = FetchType.LAZY)
    private List<Demande> demandes;

}
