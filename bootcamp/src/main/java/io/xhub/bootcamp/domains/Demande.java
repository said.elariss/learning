package io.xhub.Bootcamp.domains;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.xhub.Bootcamp.enums.Status;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Demande {
    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    private Date departureDate;
    private Date returnDate;
//    @Enumerated(EnumType.STRING)
    private Status status;
    private Integer nbrDays;
    @ManyToOne(fetch = FetchType.LAZY )
    @JsonIgnore
    private Employe employe;
}
