package io.xhub.Bootcamp.security;

import io.xhub.Bootcamp.domains.Admin;
import io.xhub.Bootcamp.domains.Employe;
import io.xhub.Bootcamp.repositories.AdminRepository;
import io.xhub.Bootcamp.repositories.EmployeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class UserDetailsServiceImp implements UserDetailsService {

    private final AdminRepository adminRepository;
    private final EmployeRepository employeRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (username.equals("admin")) {

            Optional<Admin> user = adminRepository.findByUsername(username);
            if (user.isPresent()) {
                return new User(username, user.get().getPassword(), Arrays.asList("ROLE_EMPLOYE", "ROLE_ADMIN").stream().map(SimpleGrantedAuthority::new).collect(Collectors.toSet()));
            } else {
                throw new UsernameNotFoundException("User not found");
            }

        } else {
            Optional<Employe> user = employeRepository.findByEmail(username);
            if (user.isPresent()) {
                return new User(username, user.get().getPassword(), Set.of(new SimpleGrantedAuthority("ROLE_EMPLOYE")));
            } else {
                throw new UsernameNotFoundException("User not found");
            }
        }
    }
}
