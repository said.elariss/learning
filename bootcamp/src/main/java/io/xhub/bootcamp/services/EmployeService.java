package io.xhub.Bootcamp.services;

import io.xhub.Bootcamp.domains.Demande;
import io.xhub.Bootcamp.domains.Employe;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface EmployeService {
    String addEmploye(Employe employe);

    ResponseEntity<?> removeEmploye(String id);

    Employe getProfileInformations(String email);

    ResponseEntity<?> updateEmploye(Employe employe, String id);

    ResponseEntity<?> addDemande(Demande demande, String emailEmploye);

    List<Demande> getAllDemandes();

    ResponseEntity<List<Demande>> getAllDemandesByEmploye(String emailEmploye);

    ResponseEntity<Boolean> acceptOrRejectDemande(String id, Boolean isAccepted);
}
