package io.xhub.Bootcamp.services;

import io.xhub.Bootcamp.domains.Demande;
import io.xhub.Bootcamp.domains.Employe;
import io.xhub.Bootcamp.enums.Status;
import io.xhub.Bootcamp.repositories.DemandeRepository;
import io.xhub.Bootcamp.repositories.EmployeRepository;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@RequiredArgsConstructor
@Service
public class EmployeServiceImp implements EmployeService {

//    private final AuthenticationManager authenticationManager;
    private final EmployeRepository employeRepository;
    private final DemandeRepository demandeRepository;
    private final PasswordEncoder passwordEncoder;

    public String addEmploye(Employe employe){

        try {
            employe.setPassword(passwordEncoder.encode(employe.getPassword()));
            employeRepository.save(employe);
            return "the employe has been added successfully";
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
            return "Failed";
        }
    }

    public ResponseEntity<?> removeEmploye(String id){
        Optional<Employe> emp = employeRepository.findById(id);
        if (emp.isPresent()) {
            employeRepository.delete(emp.get());
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    public Employe getProfileInformations(String email){
        Optional<Employe> employe = employeRepository.findByEmail(email);
        if (employe.isPresent()) {
            return employe.get();
        } else {
            throw new RuntimeException("user not found !!!");
        }
    }

    public ResponseEntity<?> updateEmploye(Employe employe,String id){
        Optional<Employe> emp = employeRepository.findById(id);
        if (emp.isPresent()) {
            Employe e = emp.get();
            e.setCnss(employe.getCnss());
            e.setAdress(employe.getAdress());
            e.setCin(employe.getCin());
            e.setEmail(e.getEmail());
            e.setBirthDate(employe.getBirthDate());
            e.setFirstName(employe.getFirstName());
            e.setLastName(employe.getLastName());
            e.setPhoneNumber(employe.getPhoneNumber());
            e.setSalary(employe.getSalary());
            employeRepository.save(e);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    public ResponseEntity<?> addDemande(Demande demande,String emailEmploye){
        Optional<Employe> e = employeRepository.findByEmail(emailEmploye);
        if (e.isPresent()) {
            Employe emp = e.get();
            emp.getDemandes().add(demande);
            demande.setEmploye(emp);
            employeRepository.save(emp);
            demandeRepository.save(demande);
            return ResponseEntity.ok().build();
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    public List<Demande> getAllDemandes(){
        return demandeRepository.findAll();
    }

    public ResponseEntity<List<Demande>> getAllDemandesByEmploye(String emailEmploye){
        Optional<Employe> byEmail = employeRepository.findByEmail(emailEmploye);
        if (byEmail.isPresent()) {
            return ResponseEntity.ok().body(byEmail.get().getDemandes());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    public ResponseEntity<Boolean> acceptOrRejectDemande(String id,Boolean isAccepted){
        Optional<Demande> optionalDemande = demandeRepository.findById(id);
        if (optionalDemande.isPresent()) {
            Demande demande = optionalDemande.get();
            Status status = isAccepted ? Status.ACCEPTED : Status.REJECTED;
            demande.setStatus(status);
            demandeRepository.save(demande);
            return ResponseEntity.ok().body(true);
        } else {
            return ResponseEntity.badRequest().build();
        }
    }
}
