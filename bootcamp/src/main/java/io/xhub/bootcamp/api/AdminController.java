package io.xhub.Bootcamp.api;

import io.xhub.Bootcamp.domains.Demande;
import io.xhub.Bootcamp.domains.Employe;
import io.xhub.Bootcamp.enums.Status;
import io.xhub.Bootcamp.repositories.DemandeRepository;
import io.xhub.Bootcamp.repositories.EmployeRepository;
import io.xhub.Bootcamp.services.EmployeService;
import lombok.RequiredArgsConstructor;
import org.apache.catalina.User;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class AdminController {
    private final EmployeService employeService;


    @PreAuthorize("hasAuthority('ROLE_EMPLOYE')")
    @GetMapping("/mySession")
    public Authentication getMySession(Authentication authentication) {
        return authentication;
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @PostMapping("/addEmploye")
    public String addEmploye(@RequestBody Employe employe) {
        return employeService.addEmploye(employe);
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @DeleteMapping("/removeEmploye/{id}")
    public ResponseEntity<?> removeEmploye(@PathVariable String id) {
        return employeService.removeEmploye(id);
    }

//    @PostMapping("/authenticate")
//    public Authentication authenticate(@RequestBody String username, @RequestBody String password) {
//        Authentication authenticationRequest = UsernamePasswordAuthenticationToken.unauthenticated(username, password);
//        Authentication authenticatedUSer = authenticationManager.authenticate(authenticationRequest);
//        SecurityContext sc = SecurityContextHolder.getContext();
//        sc.setAuthentication(authenticatedUSer);
//        return authenticatedUSer;
//    }


    @PreAuthorize("hasAuthority('ROLE_EMPLOYE')")
    @GetMapping("/myProfile")
    public Employe getProfileInformations(Authentication authentication) {
        String email = authentication.getName();
        return employeService.getProfileInformations(email);

    }


    @PreAuthorize("hasRole('EMPLOYE')")
    @PatchMapping("/{id}")
    public ResponseEntity<?> updateEmploye(@RequestBody Employe employe, @PathVariable String id) {
        return employeService.updateEmploye(employe, id);
    }

    @PreAuthorize("hasRole('EMPLOYE')")
    @PostMapping("/newDemande")
    public ResponseEntity<?> addDemande(@RequestBody Demande demande, Authentication authentication) {
        String emailEmploye = authentication.getName();
        return employeService.addDemande(demande, emailEmploye);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/demandes")
    public List<Demande> getAllDemandes() {
        return employeService.getAllDemandes();
    }

    @PreAuthorize("hasRole('EMPLOYE')")
    @GetMapping("/myDemandes")
    public ResponseEntity<List<Demande>> getAllDemandesByEmploye(Authentication authentication) {
        String emailEmploye = authentication.getName();
        return employeService.getAllDemandesByEmploye(emailEmploye);

    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/demandes/{id}")
    public ResponseEntity<Boolean> acceptOrRejectDemande(@PathVariable String id, @RequestBody Map<String, Boolean> request) {

        return employeService.acceptOrRejectDemande(id, request.get("isAccepted"));
    }

}
