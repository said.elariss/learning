# Transactions/Databases

Les transactions jouent un rôle crucial dans l'univers des bases de données relationnelles. Une transaction se définit comme un ensemble d'opérations traitées comme une entité indivisible, et elle doit respecter les principes ACID (Atomicité, Cohérence, Isolation et Durabilité).

- Atomicité : La transaction est traitée comme une unité indivisible. Cela signifie que toutes les opérations requises par la transaction sont exécutées dans leur intégralité, ou aucune opération n'est effectuée du tout. Il s'agit du principe "tout ou rien".

- Cohérence : La base de données reste dans un état cohérent avant et après l'exécution de la transaction. Cela garantit que toutes les contraintes d'intégrité et les règles métier sont respectées à tout moment.

- Isolation : Chaque transaction est exécutée de manière isolée des autres transactions concurrentes. Cela signifie que même si plusieurs transactions s'exécutent simultanément, elles ne doivent pas interférer les unes avec les autres. Chaque transaction doit percevoir l'état de la base de données comme si elle était la seule transaction en cours d'exécution.

- Durabilité : Une fois qu'une transaction est confirmée (commit), les modifications qu'elle a apportées doivent être permanentes et ne doivent pas être perdues, même en cas de panne du système. La base de données doit être durablement mise à jour pour refléter les résultats de la transaction.

## @Transactional
Spring nous permet de gérer les méthodes en tant que transactions grâce à l'annotation @Transactional. Cette annotation peut être appliquée soit à une classe entière, ce qui signifie que toutes les méthodes de cette classe sont considérées comme des transactions, soit à une méthode spécifique.
