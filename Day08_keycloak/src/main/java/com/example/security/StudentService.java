package com.example.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    StudentRepository studentRepository;

    public String addStudent(Student student){
        student.setPassword(passwordEncoder.encode(student.getPassword()));
        studentRepository.save(student);
        return "student is added";
    }

    public List<Student> getAllStudents(){
        return studentRepository.findAll();
    }

    public String delete(String studentName) {
        Optional<Student> student = studentRepository.findByName(studentName);
        if(student.isPresent()){
            studentRepository.delete(student.get());
            return "student was deleted";
        } else {
            return "Student Not found";
        }
    }
}
