package com.example.security;

import jakarta.persistence.*;
import jakarta.persistence.EntityManager;
import lombok.NoArgsConstructor;
import org.hibernate.SessionFactory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String password;
    private String roles;


}
