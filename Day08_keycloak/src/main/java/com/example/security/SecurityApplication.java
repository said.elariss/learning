package com.example.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class SecurityApplication {
    @Autowired
    private PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(SecurityApplication.class, args);
        System.out.println("*************************************");
        Arrays.stream(context.getBeanDefinitionNames()).forEach(System.out::println);
        System.out.println("*************************************");


    }

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository studentRepository){
        return args -> {
            List<Student> students =  Arrays.asList(new Student(null,"said",passwordEncoder.encode("1234"),"ROLE_USER,ROLE_ADMIN"),
                    new Student(null,"youssef",passwordEncoder.encode("1234"),"USER"));



            studentRepository.saveAll(students);

        };

    }

}
