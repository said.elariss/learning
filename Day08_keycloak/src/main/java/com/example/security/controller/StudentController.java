package com.example.security.controller;


import com.example.security.Student;
import com.example.security.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('USER')")
    public List<Student> getAll(){
        return studentService.getAllStudents();
    }


    @PostMapping("/new")
    @PreAuthorize("hasAuthority('USER')")
    public String addStudent(@RequestBody Student student){
        return studentService.addStudent(student);
    }

    @DeleteMapping("/delete/{studentName}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public String deleteStudent(@PathVariable String studentName ){
        return studentService.delete(studentName);
    }

    @GetMapping("mySession")
    public Authentication getSession(Authentication authentication){
        return authentication;
    }

}
