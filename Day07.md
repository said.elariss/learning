# Keycloak

Keycloak est une application JEE qui peut gérer la sécurité d'autres applications. Elle prend en charge plusieurs protocoles, dont OAuth2, OpenID et OpenID Connect.

OAuth2 est un protocole qui gère les autorisations. 

OpenID est une couche logicielle supplémentaire qui utilise les jetons JWT (JSON Web Token) pour l'authentification.

Un JWT est constitué de trois parties séparées par un point (.) :

header: Contient généralement l'algorithme utilisé pour calculer la signature et le type de jeton (ici, JWT).
payload: Contient de nombreuses informations, telles que l'ID utilisateur, le serveur ayant généré le jeton, la date d'expiration, les rôles de l'utilisateur et la date de génération du jeton.
Signature: Chaîne de caractères utilisée pour vérifier que le jeton n'a pas été modifié. Elle est calculée à partir de l'algorithme spécifié dans l'en-tête et utilise généralement des algorithmes de cryptographie comme RSA et HMAC.
La fiabilité d'un JWT réside dans sa signature numérique. Cette signature permet de garantir l'intégrité du jeton et de s'assurer qu'il n'a pas été falsifié.

