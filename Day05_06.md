# Functional Programming

La programmation fonctionnelle en JavaScript consiste à éviter les effets de bord en utilisant des fonctions pures, qui dépendent uniquement de leurs entrées. Elle encourage également l'utilisation de fonctions d'ordre supérieur, qui acceptent des fonctions en entrée et/ou retournent des fonctions en sortie. De plus, elle privilégie l'utilisation de données immuables.
# React
Dans React, dans la majorité des cas, on utilise JSX, une syntaxe qui combine du HTML et du JavaScript. JSX n'est pas compréhensible par le navigateur, mais grâce à Babel, il peut être transformé en code JavaScript compréhensible par le navigateur. Il est important de noter que dans JSX, on ne peut utiliser que des expressions JavaScript et non des déclarations (comme les instructions if, for, etc.).

`useState` est un hook qui permet d'initialiser les variables d'un composant. Si une fonction fléchée est passée en argument à `useState`, elle ne sera exécutée qu'une seule fois, lors du premier rendu du composant.

`useEffect` est un hook qui s'exécute toujours lors du premier rendu du composant. Il prend en paramètres une fonction correspondant au traitement à effectuer, et un deuxième paramètre correspondant à un tableau des dépendances. Si l'une de ces dépendances change, le traitement sera déclenché. Si `useEffect` ne dispose pas de dépendances, il sera exécuté à chaque rendu du composant, et il s'exécute de manière asynchrone.

`useRef` est utilisé pour manipuler des éléments spécifiques du DOM. Il permet aussi de référencer des valeurs.

Lors de la génération d'une liste d'éléments dans JSX, chaque élément doit être identifié par une clé unique (`key`). Il est recommandé de ne jamais utiliser les indices du tableau comme clés pour les éléments.