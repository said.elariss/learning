# React Hooks

## useState

`useState` est un hook utilisé pour gérer l'état. il retourne un tableau avec deux éléments : le premier représente l'état, et le deuxième est une fonction permettant de mettre à jour cet état.

```javascript
const [count, setCount] = useState(0);
```

## useEffect

`useEffect` est un hook qui permet d'exécuter des traitements en arrière-plan. Il prend en paramètres une fonction correspondant au traitement à effectuer, et un deuxième paramètre correspondant à un tableau des dépendances. Si l'une de ces dépendances a changé, le traitement sera déclenché.

```javascript
useEffect(()=>{
  console.log("side effect")
},[count])
```

dans la fonction à l'intérieur on peut retourner une focntion qui fait des clean up, c'est à dire que à chaque fois le traitement va s'exécuter on exécute cette fonction pour faire du nettoyage de ce que le traitement précédent a fait
```javascript
useEffect(() => {
// Effet à exécuter
// Fonction de nettoyage
return () => {
// Code de nettoyage à exécuter
};
}, [dependencies]);
```

## useMemo

`useMemo` est un hook utilisé pour mémoriser des valeurs calculées. Au lieu de recalculer une valeur qui ne changera pas à chaque ré-rendering, on mémorise cette valeur et on ne refait pas le calcul à moins que les dépendances changent.

```javascript
const doubleNumber = useMemo(() => {
  return slowFunction(number);
}, [number]);

function slowFunction(num) {
  for (let i = 0; i < 1000000000; i++) {}
  return num * 2;
}

