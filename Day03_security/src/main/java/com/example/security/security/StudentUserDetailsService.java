package com.example.security.security;

import com.example.security.Student;
import com.example.security.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StudentUserDetailsService implements UserDetailsService {
    @Autowired
    private StudentRepository studentRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Student> student = studentRepository.findByName(username);
        StudentUserDetails studentDetails = student.map(StudentUserDetails::new)
                .orElseThrow(() -> new UsernameNotFoundException("username not found"));
        studentDetails.getAuthorities().forEach(System.out::println);
        return studentDetails;
    }
}
