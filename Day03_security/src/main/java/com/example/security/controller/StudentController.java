package com.example.security.controller;


import com.example.security.Student;
import com.example.security.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping("/all")
    public List<Student> getAll(){
        return studentService.getAllStudents();
    }

    @PostMapping("/new")
    public String addStudent(@RequestBody Student student){
        return studentService.addStudent(student);
    }

    @DeleteMapping("/delete/{studentName}")
    public String deleteStudent(@PathVariable String studentName ){
        return studentService.delete(studentName);
    }

}
