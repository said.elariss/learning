package com.example.security;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
public class Student {



    @Id
    @SequenceGenerator(name = "seq_student",
    sequenceName = "seq_student",
    allocationSize = 1,
    initialValue = 5)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
    generator = "seq_student")
    private Long id;
    private String name;
    private String password;
    private String roles;

    public Student() {

    }
}
