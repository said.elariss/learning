package com.example.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class SecurityApplication {
    @Autowired
    private PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(SecurityApplication.class, args);
    }

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository studentRepository){
        return args -> {
            List<Student> students =  Arrays.asList(new Student(null,"said",passwordEncoder.encode("1234"),"ROLE_USER,ROLE_ADMIN"),
                    new Student(null,"youssef",passwordEncoder.encode("1234"),"USER"));

            studentRepository.saveAll(students);

        };

    }

}
