# Git

Git est un outil de versioning qui nous permet de travailler en collaborant avec les parties prenantes du projet de manière efficace.

## Architecture de Git :

![Architecture de Git](https://miro.medium.com/v2/da:true/resize:fit:1200/0*0ADwbsA30R2Azaa0)

- Le premier s'agit du **working directory**, c'est le répertoire dans lequel on travaille.
- Le deuxième s'agit de **staging area** qui est une étape intermédiaire avant de faire le commit.
- Le dernier c'est le **repository** qui est un répertoire dans lequel on met nos commits.

## Les principales commandes de Git : 

- `git init` : pour initialiser un répertoire Git.
- `git add` : pour ajouter des fichiers dans la zone de staging area.
- `git commit` : pour stocker les changements qu'on a faits sous forme d'une "version" dans le repository.
- `git branch` : pour lister l'ensemble des branches existantes dans notre répertoire.
- `git log` : pour visualiser l'ensemble des commits de la branche en cours.
- `git checkout` : pour switcher d'une branche à une autre.
- `git clean` : pour supprimer les fichiers non suivis (ceux qui n'existent pas dans le staging area).
- `git clone` : pour cloner un projet à partir d'un dépôt distant vers notre dépôt local.
- `git commit --amend` : une commande qui permet de changer le message du dernier commit, et aussi on peut effectuer des modifications sur le dernier commit sans avoir besoin de faire un nouveau commit.
- `git merge` : permet de fusionner les commits d'une branche dans une autre branche.
- `git merge --squash` : regrouper l'ensemble des commits en un seul commit et le fusionner dans une autre branche.
- `git fetch` : permet de récupérer des référentiels distants du dépôt distant et les stocker localement dans un espace réservé au refs distantes.
- `git pull` : `git fetch` + `git merge`.
- `git rebase` : regrouper les commits d'une branche avec une autre branche d'une manière linéaire.

# JUnit

En Java, on utilise principalement la bibliothèque JUnit pour faire du testing.

## Architecture de JUnit :

![Architecture de JUnit](https://kkjavatutorials.com/wp-content/uploads/2019/11/JUnit-Architecture.jpg)

- **JUnit Platform** : c'est celui qui lance les tests et expose des APIs pour nous permettre d'exécuter les tests.
- **JUnit Vintage** : c'est un module qui assure le backward compatibility, c'est-à-dire qu'on peut exécuter des codes de JUnit 3,4.
- **JUnit Jupiter** : c'est un module qui contient des nouvelles annotations de JUnit 5 comme `@Disabled`, `@AfterAll`, `@BeforeAll`, `@AfterEach`, `@BeforeEach`, `@DisplayName` ...

# TDD

TDD (Test-Driven Development) est une méthode qui favorise l'écriture des tests unitaires avant de passer aux codes. Voici les étapes du TDD :

1. Rédiger un test.
2. Faire échouer le test.
3. Écrire du code de production.
4. Exécuter les tests.
5. Refactoriser le code.
6. Répéter.

