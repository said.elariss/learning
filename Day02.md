## JPA & Spring Data JPA :

**JPA (Java Persistence API)** est une **spécification** qui gère tout ce qui est relation à l'accès aux données .

**Parmi les implémentations de JPA, on trouve Hibernate**, qui est un framework ORM (Object-Relational Mapping).

**Spring Data JPA** est une abstraction de niveau supérieur à JPA qui **facilite l'utilisation de JPA** et utilise Hibernate par défaut pour l'accès aux bases de données relationnelles.

![JPA](https://miro.medium.com/v2/resize:fit:454/1*-u5aLfuU7XhtUUZDbs2G3A.png)

**Parmi les principales interfaces fournies par JPA, on trouve :**

* **JpaRepository** : Cette interface hérite de l'interface **PagingAndSortingRepository**, qui hérite elle-même de **CrudRepository**. Ces interfaces fournissent des méthodes génériques pour effectuer des opérations CRUD (Create, Read, Update, Delete) sur des entités JPA.

**Parmi les principales annotations de JPA :**

* **@Entity** : Indique qu'une classe est une entité JPA et qu'elle peut être persistée dans une base de données.
* **@Table** : Permet de spécifier le nom de la table dans la base de données qui correspond à l'entité.
* **@Id** : Déclare un champ comme clé primaire.
* **@GeneratedValue** : Permet de générer automatiquement les valeurs de la clé primaire.
* **@ManyToOne** : Défini une relation "un à plusieurs" entre deux entités.
* **@OneToMany** : Défini une relation "un à plusieurs" entre deux entités.
* **@ManyToMany** : Défini une relation "plusieurs à plusieurs" entre deux entités.

# JavaScript ES6

## Portée des variables

La portée définit où les variables sont définies et où elles peuvent être accédées.

- `var`, `let`, `const` :
  - `var` crée des variables avec une portée de fonction.
  - `let` crée des variables avec une portée de bloc.
  - `const` crée des constantes dont la valeur ne peut pas être réassignée.

## Types de données primitifs et non primitifs

Les types de données primitifs peuvent contenir une seule valeur et sont immuables.

## Le "Hoisting"

Le "hoisting" est un comportement particulier où les déclarations de variables et de fonctions sont déplacées vers le haut de leur contexte d'exécution avant l'exécution réelle du code. Cela signifie qu'on peut utiliser une variable ou une fonction avant de les avoir déclarées explicitement dans notre code.

## Différence entre l'opérateur Spread et Rest

- **Spread Operator** : permet de distribuer les éléments d'un tableau.
  ```javascript
  const tableau1 = [1, 2, 3];
  const tableau2 = [...tableau1, 4, 5, 6];
  let arr = [1, 8, 9, 7, 44, 13];
  Math.max(...arr);

- **Rest Operator** :  permet de rassembler les élements dans un tableau.
  ```javascript
  const [premier, ...reste] = [1, 2, 3, 4, 5]
    function f(...args){
        ///
    }
